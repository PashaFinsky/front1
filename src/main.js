import Vue from 'vue'
import App from './App'
import router from './router'
import axios from "axios";

Vue.config.productionTip = false


function SubmitData(name, surname)
{
  axios
    .post('http://localhost:3000/UserInput', {
      name: name,
      surname: surname
    })
}

Vue.directive('my-validation',
  {
    update: function (el, binding)
    {
      el.onclick = function (evt)
      {
        let valid = true;
        for (let x = 0; x < binding.value.info.length; x++)
        {
          if (binding.value.name.toUpperCase() === binding.value.info[x].name.toUpperCase() && binding.value.surname.toUpperCase() === binding.value.info[x].surname.toUpperCase())
          {
            alert("This user is already registered")
            valid = false
            evt.stopPropagation()
            break
          }
        }
        if (valid)
        {
          SubmitData(binding.value.name, binding.value.surname)
          alert("Uspeshnaya registraci9")
        }
      }
    }
  })

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
